from django.db import models

# Create your models here.


class User(models.Model):
    """
    autor: Felix Micolta
    date: 11/30/2019
    description: Basic User model
    """
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name
