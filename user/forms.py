from django import forms


class UserForm(forms.Form):
    """
    autor: Felix Micolta
    date: 11-30-2019
    description: Form to create names of users for game
    """
    user_1 = forms.CharField(max_length=30, required=True)
    user_2 = forms.CharField(max_length=30, required=True)

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)

        self.fields['user_1'].label = "Player 1"
        self.fields['user_2'].label = "Player 2"
