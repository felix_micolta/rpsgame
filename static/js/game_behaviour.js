///logic behaviour of turns



$("#ok_player_1").on('click', function () {
    var player_choice = $("#id_player_choice :selected").val();
    $("#player2_name").show();
    $("#player2_button").show();
    $("#player1_name").hide();
    $("#player1_button").hide();
    $('#id_player_choice').val(0);
    $.post(url, {'player_choice': player_choice, 'player': 1, 'game': game, 'csrfmiddlewaretoken': token},
        function(response){
            console.log(response);
        }
    );

});


$("#ok_player_2").on('click', function () {
    var player_choice = $("#id_player_choice :selected").val();
    $("#player2_name").hide();
    $("#player2_button").hide();
    $("#player1_name").show();
    $("#player1_button").show();
    $('#id_player_choice').val(0);
    $.post(url, {'player_choice': player_choice, 'player': 2 , 'game': game, 'csrfmiddlewaretoken': token},
        function(response){
            console.log(response);
            if("game_winner" in response){
                location.href=url_game_winner;
                //alert("Winner"+ response['game_winner']);
            }else if("score" in response){
                $('#score_list').empty();
                var round = 1;
                for(var i=0; i<response['score'].length; i++){
                    $("#score_list").append('<li>'+round+"-"+response['score'][i]+'</li>');
                    round ++;
                }
                $('#round_number').text(round);

            }

        }
    );
});