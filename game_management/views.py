from django.views.generic import TemplateView, FormView
from django.core.urlresolvers import reverse
from django.http import JsonResponse

from game_management.constants import tie, rock, scissors, paper
from game_management.forms import PlayerChoiceForm
from game_management.models import Game, Round
from user.forms import UserForm
from user.models import User


class StartGameView(FormView):
    """
    autor: Felix Micolta
    date: 11-30-2019
    description: This view create a game
    """
    template_name = 'index.html'
    form_class = UserForm
    game = None

    def form_valid(self, form):
        user1 = form.cleaned_data['user_1']
        user2 = form.cleaned_data['user_2']

        player1 = User.objects.create(name=user1)
        player2 = User.objects.create(name=user2)

        self.game = Game.objects.create(user_1=player1, user_2=player2)

        return super(StartGameView, self).form_valid(form)

    def get_success_url(self):

        return reverse('game_management:load_game',
                       kwargs={'pk': self.game.pk})


class ChoiceView(FormView):
    """
    autor: Felix Micolta
    date: 11-30-2019
    description: This view create a game
    """
    template_name = 'playing_screen.html'
    form_class = PlayerChoiceForm

    def get_context_data(self, **kwargs):
        context = super(ChoiceView, self).get_context_data(**kwargs)

        game = Game.objects.filter(pk=self.kwargs['pk']).first()

        context['player_1'] = game.user_1.name
        context['player_2'] = game.user_2.name
        context['game'] = game.pk
        actual_round = Round.objects.filter(game=self.kwargs['pk']).count()
        if actual_round == 0:
            context['actual_round'] = 1
        else:
            context['actual_round'] = actual_round + 1
        context['score'] = game.get_rounds_by_game()
        return context


class WinnerView(TemplateView):
    """
    autor: Felix Micolta
    date: 11-30-2019
    description: This view show the game winner
    """
    template_name = 'winner_screen.html'
    form_class = PlayerChoiceForm

    def get_context_data(self, **kwargs):
        context = super(WinnerView, self).get_context_data(**kwargs)

        last_game = Game.objects.last()
        winner = User.objects.filter(pk=last_game.game_winner).first()
        context['winner'] = winner
        return context


def validate_round(round):
    """
    autor: Felix Micolta
    date: 11-30-2019
    description: Main logic of game
    :param round:
    :return: actually winner or tie
    """
    player_1_choice = int(round.user1_choice)
    player_2_choice = int(round.user2_choice)

    if player_1_choice == player_2_choice:
        round.round_winner = None
        return tie

    if player_1_choice == rock:
        if player_2_choice == scissors:
            round.round_winner = round.game.user_1
        elif player_2_choice == paper:
            round.round_winner = round.game.user_2
    elif player_1_choice == paper:
        if player_2_choice == rock:
            round.round_winner = round.game.user_1
        elif player_2_choice == scissors:
            round.round_winner = round.game.user_2
    elif player_1_choice == scissors:
        if player_2_choice == paper:
            round.round_winner = round.game.user_1
        elif player_2_choice == rock:
            round.round_winner = round.game.user_2
    round.save()
    return round.get_round_winner()


def validate_game(winner, game):
    """
    autor: Felix Micolta
    date: 11-30-2019
    description: This function helps to know if exist a game winner.
    :param winner:
    :param game:
    :return:
    """
    wins_rounds = game.get_rounds_wins_by_player(winner)

    if wins_rounds > 2:
        return winner

    return None


def save_move(request):
    """
    autor: Felix Micolta
    date: 11-30-2019
    description: Save a player move
    :param request:
    :return:
    """
    data = {}
    if request.is_ajax():
        game_winner = None
        player_choice = request.POST['player_choice']
        player = request.POST['player']
        game = request.POST['game']
        game_obj = Game.objects.filter(pk=game).first()

        if int(player) == 1:
            round = Round.objects.create(user1_choice=player_choice,
                                         game=game_obj)
            round.save()
        elif int(player) == 2:
            active_round = Round.objects.filter(user2_choice=None,
                                                game=game).last()
            active_round.user2_choice = player_choice
            active_round.save()
            round_winner = validate_round(active_round)
            data = {'score': game_obj.get_rounds_by_game()}
            if round_winner is not tie:
                game_winner = validate_game(round_winner, game_obj)

        if game_winner is not None:
            game_obj.game_winner = game_winner.pk
            game_obj.save()
            data = {'game_winner': game_winner.pk}

    return JsonResponse(data)
