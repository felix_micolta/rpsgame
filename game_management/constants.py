rock = 0
scissors = 1
paper = 2

win = 0
tie = 1
lose = 2

GAME_CHOICES = [
    (rock, 'Rock'),
    (scissors, 'Scissors'),
    (paper, 'Paper')
]
