from django.test import TestCase

from game_management.models import Game, Round
from user.models import User


class GameTest(TestCase):
    """
    autor: Felix Micolta
    date: 12-1-2019
    description: Test case for Game model
    """

    @classmethod
    def setUpTestData(cls):
        user1 = User.objects.create(name="user1")
        user1.save()
        user2 = User.objects.create(name="user2")
        user2.save()
        game = Game.objects.create(user_1=user1, user_2=user2)
        Round.objects.create(game=game, user1_choice=0, user2_choice=1,
                             round_winner=user1)
        Round.objects.create(game=game, user1_choice=0, user2_choice=0)

    def test_get_game_winner(self):
        user1 = User.objects.get(pk=1)
        game = Game.objects.get(pk=1)
        game.game_winner = user1.pk
        game.save()
        winner = game.get_game_winner()
        user_winner = User.objects.filter(pk=game.game_winner).first()
        self.assertEquals(
            winner,
            user_winner.name
        )

    def test_get_game_without_winner(self):

        user1 = User.objects.create(name="user1")
        user2 = User.objects.create(name="user2")
        game = Game.objects.create(user_1=user1, user_2=user2)
        winner = game.get_game_winner()
        user_winner = User.objects.filter(pk=game.game_winner).first()
        self.assertEquals(
            winner,
            user_winner
        )

    def test_get_rounds_by_game(self):
        game = Game.objects.get(pk=1)
        rounds_of_game = game.get_rounds_by_game()

        score = []
        rounds_by_game = Round.objects.filter(game=game.pk).order_by('pk')
        for round in rounds_by_game:
            if round.round_winner:
                score.append(round.round_winner.name)
            else:
                score.append("Tie")

        self.assertEquals(
            rounds_of_game,
            score
        )

    def test_get_rounds_wins_by_player(self):
        game = Game.objects.get(pk=1)
        user1 = User.objects.get(pk=1)
        rounds_wins = game.get_rounds_wins_by_player(user1)

        rounds = Round.objects.filter(game=game, round_winner=user1).count()

        self.assertEquals(
            rounds_wins,
            rounds
        )


class RoundTest(TestCase):
    """
    autor: Felix Micolta
    date: 12-1-2019
    description: Test case for Round model
    """

    @classmethod
    def setUpTestData(cls):
        user1 = User.objects.create(name="user1")
        user1.save()
        user2 = User.objects.create(name="user2")
        user2.save()
        game = Game.objects.create(user_1=user1, user_2=user2)
        Round.objects.create(game=game, user1_choice=0, user2_choice=1,
                             round_winner=user1)
        Round.objects.create(game=game, user1_choice=0, user2_choice=0)

    def test_get_round_winner(self):
        round = Round.objects.first()

        round_winner = round.get_round_winner()

        self.assertEquals(
            round_winner,
            round.round_winner
        )
