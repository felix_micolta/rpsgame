from django.test import TestCase
from django.core.urlresolvers import reverse


from game_management.models import Game, Round
from user.models import User


class StartGameViewTest(TestCase):
    """
    autor: Felix Micolta
    date: 12-1-2019
    description: TestCase for StartGameView
    """

    def setUp(self):
        self.url = reverse('start_game')

    def tearDown(self):
        del self.url

    def test_views_200(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description: method to verify 302 redirect status StartGameView
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_views_302(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description: method to verify 302 redirect status StartGameView
        """
        data = {
            'user_1': "Felix",
            'user_2': "Felix Friend's"
        }

        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, 302)


class ChoiceViewTest(TestCase):
    """
    autor: Felix Micolta
    date: 12-1-2019
    description: TestCase for ChoiceView
    """

    def setUp(self):
        user1 = User.objects.create(name="user1")
        user1.save()
        user2 = User.objects.create(name="user2")
        user2.save()
        game = Game.objects.create(user_1=user1, user_2=user2)
        Round.objects.create(game=game, user1_choice=0, user2_choice=1,
                             round_winner=user1)
        Round.objects.create(game=game, user1_choice=0, user2_choice=0)
        self.url = reverse('game_management:load_game', kwargs={'pk': game.pk})

    def tearDown(self):
        del self.url

    def test_views_200(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description: method to verify 200 redirect status ChoiceView
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)

    def test_views_200_without_round(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description: method to verify 200 redirect status ChoiceView without
         a round
        """
        rounds = Round.objects.all()
        rounds.delete()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)


class WinnerViewTest(TestCase):
    """
    autor: Felix Micolta
    date: 12-1-2019
    description: TestCase for WinnerView
    """

    def setUp(self):
        user1 = User.objects.create(name="user1")
        user1.save()
        user2 = User.objects.create(name="user2")
        user2.save()
        game = Game.objects.create(user_1=user1, user_2=user2,
                                   game_winner=user1.pk)
        Round.objects.create(game=game, user1_choice=0, user2_choice=1,
                             round_winner=user1)
        Round.objects.create(game=game, user1_choice=0, user2_choice=0)
        self.url = reverse('game_management:winner_view')

    def tearDown(self):
        del self.url

    def test_views_200(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description: method to verify 200 redirect status WinnerView
        """
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)


class SaveMoveTest(TestCase):
    """
    autor: Felix Micolta
    date: 12-1-2019
    description: TestCase for save_move
    """
    def setUp(self):
        user1 = User.objects.create(name="user1")
        user1.save()
        user2 = User.objects.create(name="user2")
        user2.save()
        self.user2 = user2.pk
        self.game = Game.objects.create(user_1=user1, user_2=user2)
        self.url = reverse('game_management:next_game_step')
        self.data = {
            'player_choice': 0,
            'player': 1,
            'game': self.game.pk
        }

    def tearDown(self):
        del self.url

    def test_ajax_200(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description: method to test 200 status with get with data
        """

        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_1(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2

        Round.objects.create(game=self.game, user1_choice=0)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_2(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2

        Round.objects.create(game=self.game, user1_choice=1)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_3(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2

        Round.objects.create(game=self.game, user1_choice=2)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_4(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2
        self.data['player_choice'] = 2

        Round.objects.create(game=self.game, user1_choice=0)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_5(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2
        self.data['player_choice'] = 1

        Round.objects.create(game=self.game, user1_choice=0)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_6(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2
        self.data['player_choice'] = 2

        Round.objects.create(game=self.game, user1_choice=0)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_7(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2
        self.data['player_choice'] = 1

        Round.objects.create(game=self.game, user1_choice=2)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_8(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2
        self.data['player_choice'] = 0

        Round.objects.create(game=self.game, user1_choice=2)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_move_9(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2
        self.data['player_choice'] = 2

        Round.objects.create(game=self.game, user1_choice=1)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)

    def test_ajax_player_2_wins(self):
        """
        autor: Felix Micolta
        date: 12-1-2019
        description:
        """
        self.data['player'] = 2
        self.data['player_choice'] = 2
        user2 = User.objects.get(pk=self.user2)
        Round.objects.create(game=self.game, user1_choice=0, user2_choice=2,
                             round_winner=user2)
        Round.objects.create(game=self.game, user1_choice=0, user2_choice=2,
                             round_winner=user2)
        Round.objects.create(game=self.game, user1_choice=0)
        response = self.client.post(self.url,
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest',
                                    data=self.data)
        self.assertEqual(response.status_code, 200)
