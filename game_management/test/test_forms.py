from django.test import TestCase

from game_management.forms import PlayerChoiceForm


class PlayerChoiceFormTest(TestCase):
    """
    autor: Felix Micolta
    date: 12-1-2019
    description: Test Case for PlayerChoiceForm
    """

    def setUp(self):
        self.dict = {
            'player_choice': 0,
        }

    def test_form_valid(self):
        form = PlayerChoiceForm(data=self.dict)
        self.assertTrue(form.is_valid())

    def test_form_invalid(self):
        form = PlayerChoiceForm(data=None)
        self.assertFalse(form.is_valid())
