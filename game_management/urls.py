
from django.conf.urls import url

from game_management.views import ChoiceView, save_move, WinnerView

urlpatterns = [
    url(r'^playing-game/(?P<pk>\d+)/$', ChoiceView.as_view(),
        name='load_game'),
    url(r'^win-view/$', WinnerView.as_view(), name='winner_view'),
    url(r'^next-step/$', save_move, name='next_game_step'),
]
