from django import forms

from game_management.constants import GAME_CHOICES


class PlayerChoiceForm(forms.Form):
    """
    autor: Felix Micolta
    date: 11-30-2019
    description: Form to play the user move
    """
    player_choice = forms.IntegerField(required=True,
                                       widget=forms.Select(
                                           choices=GAME_CHOICES))

    def __init__(self, *args, **kwargs):
        super(PlayerChoiceForm, self).__init__(*args, **kwargs)

        self.fields['player_choice'].label = "Select Move"
        self.fields['player_choice'].widget.attrs.update({
            'class': 'custom-select'})
