from django.db import models

# Create your models here.
from game_management.constants import GAME_CHOICES
from user.models import User


class Game(models.Model):
    """
    autor: Felix Micolta
    date: 30-11-2019
    description: Game Model
    """
    user_1 = models.ForeignKey('user.User', verbose_name='User 1',
                               related_name='user_one',
                               null=True, blank=True)
    user_2 = models.ForeignKey('user.User', verbose_name='User 2',
                               related_name='user_two',
                               null=True, blank=True)
    game_winner = models.PositiveIntegerField(null=True, blank=True)

    def get_game_winner(self):
        """
        autor: Felix Micolta
        date: 30-11-2019
        description: This function return the game winner name
        :return: winner.name charfield
        """
        if self.game_winner:
            winner = User.objects.filter(pk=self.game_winner).first()
            return winner.name

        return None

    def get_rounds_by_game(self):
        """
        autor: Felix Micolta
        date: 30-11-2019
        description: This function returns the score of a game
        :param game: game pk
        :return: score list
        """
        score = []
        rounds_by_game = Round.objects.filter(game=self.pk).order_by('pk')
        for round in rounds_by_game:
            if round.round_winner:
                score.append(round.round_winner.name)
            else:
                score.append("Tie")

        return score

    def get_rounds_wins_by_player(self, user):
        """
        autor: Felix Micolta
        date: 30-11-2019
        description: This function count how many rounds wins a player by game
        :param game: game pk
        :param user: user pk
        :return: rounds integer
        """
        rounds = Round.objects.filter(game=self.pk, round_winner=user)

        return rounds.count()


class Round(models.Model):
    """
    autor: Felix Micolta
    date: 30-11-2019
    description: Round Model
    """
    user1_choice = models.PositiveIntegerField(choices=GAME_CHOICES)
    user2_choice = models.PositiveIntegerField(choices=GAME_CHOICES, null=True)
    round_winner = models.ForeignKey('user.User', verbose_name='Round Winner',
                                     null=True, blank=True)
    game = models.ForeignKey(Game)

    def get_round_winner(self):
        """
        autor: Felix Micolta
        date: 30-11-2019
        description: This function returns winner of round
        :return: winner
        """
        return self.round_winner
