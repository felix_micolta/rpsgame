PROJECT INSTALLATION
==================================

1. CLONE REPOSITORY
=====================

git clone https://felix_micolta@bitbucket.org/felix_micolta/rpsgame.git

2. MAKE ENV
=================

$ virtualenv <env-name> -p python3

$ source <env-path>/bin/activate

4. INSTALL REQUIREMENTS
========================

pip install -r requirements.txt

4.1 If you couldn't install psycopg2 on ubuntu:
$ sudo apt install libpq-dev python3-dev
$ sudo apt install build-essential

5. MIGRATIONS (first make sure of configure database on settings.py file)
=================================================================

python manage.py makemigrations
python manage.py migrate
==========================================

6. RUN SERVER

python manage.py runserver

Aditional notes: you must to have postgres previously installed and 
create a blank data base. The installation notes apply to a linux work 
environment. The relation model of database is in 
"static/img/RPS_game_relation_model.png"

==========================================

7. RUNNING TEST

You can use python manage.py to run test

7.1 TEST COVERAGE

(coverage required)

coverage run --include='./*' manage.py test

Results on command line

    coverage report

Results on html files

    coverage html



